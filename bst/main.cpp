#include "stablo.hpp"
#include <iostream>
#include <string>
#include <queue>

int main() {
  Tree<int> moj_tree;

  moj_tree.insert(5);
  moj_tree.print();

  moj_tree.insert(1);
  moj_tree.print();

  moj_tree.insert(2);
  moj_tree.print();

  moj_tree.insert(3);
  moj_tree.print();

  moj_tree.insert(10);
  moj_tree.print();

  moj_tree.insert(11);
  moj_tree.print();

  moj_tree.insert(12);
  moj_tree.print();

  moj_tree.postorder();
  moj_tree.preorder();

  auto it = moj_tree.find(3);
  if (it) {
    std::cout << "Pronasli smo element " << it->value << std::endl;
  } else {
    std::cout << "Nismo pronasli smo element" << std::endl;
  }
  
  it = moj_tree.find(12);
  if (it) {
    std::cout << "Pronasli smo element " << it->value << std::endl;
  } else {
    std::cout << "Nismo pronasli smo element" << std::endl;
  }

  it = moj_tree.find(16);
  if (it) {
    std::cout << "Pronasli smo element " << it->value << std::endl;
  } else {
    std::cout << "Nismo pronasli smo element" << std::endl;
  }

  std::cout << "Dubina: " << moj_tree.depth() << std::endl;
 
  // std::cout << "Testiramo copy ctor" << std::endl;
  // auto moj_tree1 = moj_tree;
  // std::cout << "mojtree ";
  // moj_tree.print();
  // std::cout << "mojtree1 ";
  // moj_tree1.print();

  // std::cout << "Testiramo move ctor" << std::endl;
  // auto moj_tree2 = std::move(moj_tree);
  // std::cout << "mojtree2 ";
  // moj_tree2.print();

  // std::cout << "Testiramo copy=" << std::endl;
  // moj_tree = moj_tree1;
  // std::cout << "mojtree ";
  // moj_tree.print();

  // std::cout << "Testiramo move=" << std::endl;
  // std::cout << "mojtree ";
  // moj_tree = std::move(moj_tree1);
  // std::cout << "Print" << std::endl;
  // moj_tree.print();

  // std::cout << "Depth" << std::endl;
  // moj_tree.preorder();
  // std::cout << '\n';
  // moj_tree.inorder();
  // std::cout << "\n";
  // moj_tree.postorder();
  // std::cout << std::endl;

  // moj_tree.clear();
  // moj_tree.print();

  // moj_tree.erase(10);
  // moj_tree.print();

  // moj_tree.erase(30);
  // moj_tree.print();

  // moj_tree.erase(5);
  // moj_tree.print();

  // moj_tree.erase(3);
  // moj_tree.print();

  // moj_tree.erase(2);
  // moj_tree.print();

  // moj_tree.erase(1);
  // moj_tree.erase(11);
  // moj_tree.erase(12);
  // moj_tree.print();

  return 0;
}
