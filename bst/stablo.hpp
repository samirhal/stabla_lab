#pragma once

#include <iostream>
#include <queue>

template <typename T> struct Node {
  T value;
  Node *left_ = nullptr;
  Node *right_ = nullptr;
};

template <typename T> class Tree {
  Node<T> *root_ = nullptr;
  size_t size_ = 0;

  static void print_subtree(const Node<T> *root) {
      if (root->left_) print_subtree(root->left_);
      std::cout << root->value << " ";
      if (root->right_) print_subtree(root->right_);
  }
  static void print_preorder(const Node<T> *root) {
      std::cout << root->value << " ";
      if (root->left_) print_subtree(root->left_);
      if (root->right_) print_subtree(root->right_);
  }
  static void print_postorder(const Node<T> *root) {
      if (root->left_) print_subtree(root->left_);
      if (root->right_) print_subtree(root->right_);
      std::cout << root->value << " ";
  }

  static void clear_subtree(Node<T> *root) {
    if (root == nullptr) return;
    clear_subtree(root->left_);
    clear_subtree(root->right_);
    delete root;
  }

  static int depth(Node<T> *current) {
    if (current->left_ == nullptr && current->right_ == nullptr) {
      return 0;
    }
    int dubina_l = 0;
    int dubina_r = 0;
    if (current->left_) dubina_l = depth(current->left_);
    if (current->right_) dubina_r = depth(current->right_);
    return 1 + std::max(dubina_l, dubina_r);
  }

  void insert_subtree(Node<T>* root) {
    // iterira kroz tudje stablo
    if (root == nullptr) {
      return;
    }
    insert(root->value);
    insert_subtree(root->left_);
    insert_subtree(root->right_);
  }

  void eraseNode(Node<T>*& e) {
    if (e == nullptr) {
      return;
    } else if (e->left_ == nullptr && e->right_ == nullptr) {
      // cvor e nema dijece
      delete e;
      e = nullptr;
    } else if (e->right_ == nullptr) {
      // cvor e ima jedno dijete - left_
      auto temp = e;
      e = e->left_;
      delete temp;
    } else if (e->left_ == nullptr) {
      // cvor e ima jedno dijete - right_
      auto temp = e;
      e = e->right_;
      delete temp;
    } else {
      // cvor e ima dvoje dijece;
      Node<T>* previous = nullptr;
      auto current = e->left_;

      while (current->right_ != nullptr) {
        previous = current;
        current = current->right_;
      }

      e->value = current->value;

      if (previous != nullptr) {
        previous->right_ = current->left_;
      } else {
        e->left_ = current->left_;
      }

      delete current;
    }
    size_--;
  }

public:
  Tree() = default;
  Tree(const Tree& other) {
    insert_subtree(other.root_);
    size_ = other.size_;
  }
  Tree(Tree&& other) : root_{other.root_}, size_{other.size_} {
    other.root_ = nullptr;
    other.size_ = 0;
  }
  Tree& operator=(const Tree& other) {
    if (this == &other) return *this;
    clear();
    insert_subtree(other.root_);
    size_ = other.size_;
    return *this;
  }
  Tree& operator=(Tree&& other) {
    if (this == &other) return *this;
    clear();
    root_ = other.root_;
    size_ = other.size_;

    other.root_ = nullptr;
    other.size_ = 0;
    return *this;
  }
  ~Tree() { clear(); }

  bool size() const { return size_; }
  bool empty() const { return root_ == nullptr; }

  template <typename F> 
  void insert(F &&v) {
    if (empty()) {
      root_ = new Node<T>{std::forward<F>(v), nullptr, nullptr};
    } else {
      auto current = root_;
      while (current != nullptr) {
        if (v > current->value) {
          if (current->right_ == nullptr) {
            current->right_ = new Node<T>{std::forward<F>(v)};
            break;
          } else {
            current = current->right_;
          }
        } else {
          if (current->left_ == nullptr) {
            current->left_ = new Node<T>{std::forward<F>(v)};
            break;
          } else {
            current = current->left_;
          }
        }
      }
    }
  }

  void print() const {
    print_subtree(root_);
    std::cout << std::endl;
  }

  const Node<T> *find(const T &v) const {
    auto current = root_;
    while (current != nullptr) {
      if (v > current->value) {
        current = current->right_;
      } else if (v == current->value) {
        return current;
      } else {
        current = current->left_;
      }
    }
    return nullptr;
  }

  int depth() const { 
    return depth(root_); 
  }

  void clear() {
    clear_subtree(root_);
    size_ = 0;
    root_ = nullptr;
  }

  void erase(const T &v) {
    Node<T>* previous = nullptr;
    auto current = root_;
    while (current != nullptr) {
      if (v > current->value) {
        previous = current;
        current = current->right_;
      } else if (v == current->value) {
        if (previous->left_->value == v)
          eraseNode(previous->left_);
        else 
          eraseNode(previous->right_);
      } else {
        previous = current;
        current = current->left_;
      }
    }
  }

  void preorder() const { 
    print_preorder(root_); 
    std::cout << std::endl;
  }
  void postorder() const { 
    print_postorder(root_); 
    std::cout << std::endl;
  }
};
